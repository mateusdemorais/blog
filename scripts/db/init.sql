CREATE DATABASE blog;
CREATE USER bloguser WITH PASSWORD 'blogpass';
ALTER ROLE bloguser SET client_encoding TO 'utf8';
ALTER ROLE bloguser SET default_transaction_isolation TO 'read committed';
ALTER ROLE bloguser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE blog TO bloguser;

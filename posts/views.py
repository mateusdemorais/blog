from django.shortcuts import render, redirect

from .models import Post, Comment


def index_view(request):
    
    posts = Post.objects.all()
    context = {'posts': posts}

    return render(request, "posts.html", context)


def create_post_view(request):

    if request.method == 'POST':

        Post.objects.create(
            owner=request.user,
            body=request.POST.get("body")
        )

    return redirect('/')


def delete_post_view(request, post_id):

    if request.method == 'POST':

        post = Post.objects.get(id=post_id)

        if post.owner == request.user:

            post.delete()

    return redirect('/')


def create_comment_view(request, post_id):

    if request.method == 'POST':

        post = Post.objects.get(id=post_id)

        Comment.objects.create(
            owner=request.user,
            post=post,
            body=request.POST.get("body")
        )

    return redirect('/')


def delete_comment_view(request, comment_id):

    if request.method == 'POST':

        comment = Comment.objects.get(id=comment_id)

        if comment.owner == request.user:

            comment.delete()

    return redirect('/')

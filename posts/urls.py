from django.urls import path

from . import views

urlpatterns = [
    path('', views.index_view, name='index'),
    path('create_post', views.create_post_view, name='create_post'),
    path('delete_post/<int:post_id>', views.delete_post_view, name='delete_post'),
    path('create_comment/<int:post_id>', views.create_comment_view, name='create_comment'),
    path('delete_comment/<int:comment_id>', views.delete_comment_view, name='delete_comment'),
]

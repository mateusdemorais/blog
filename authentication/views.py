from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout


def login_view(request):

    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('/')
        else:
            return render(request, "login.html")

    elif request.method == 'POST':
        user = authenticate(
            request,
            username=request.POST.get("username"),
            password=request.POST.get("password")
        )

        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            return redirect('/')


def logout_view(request):

    if request.user.is_authenticated:
        logout(request)
        return redirect('/')
    else:
        return redirect('/')
